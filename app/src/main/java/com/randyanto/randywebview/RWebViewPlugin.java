/*
 * Copyright (C) 2011 Keijiro Takahashi
 * Copyright (C) 2012 GREE, Inc.
 * Copyright (C) 2016 Yonathan Randyanto
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

package com.randyanto.randywebview;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayer;

/**
 * Created by user on 7/4/2016.
 */

public class RWebViewPlugin {

    private static FrameLayout layout = null;
    private WebView mWebView;

    public RWebViewPlugin() {
    }

    public boolean IsInitialized() {
        return mWebView != null;
    }

    public void Init() {
        final RWebViewPlugin self = this;
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {public void run() {
            if (IsInitialized()) {
                return;
            }
            final WebView webView = new WebView(a);

            webView.setVisibility(View.VISIBLE);
            webView.setFocusable(true);
            webView.setFocusableInTouchMode(true);
            webView.setWebChromeClient(new WebChromeClient());

            webView.setWebViewClient(new WebViewClient(){});

            WebSettings webSettings = webView.getSettings();
            webSettings.setSupportZoom(false);
            webSettings.setJavaScriptEnabled(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setDomStorageEnabled(true);
            webSettings.setPluginState(WebSettings.PluginState.ON);

            if (layout == null) {
                layout = new FrameLayout(a);
                a.addContentView(
                        layout,
                        new LayoutParams(
                                LayoutParams.MATCH_PARENT,
                                LayoutParams.MATCH_PARENT)
                );
                layout.setFocusable(true);
                layout.setFocusableInTouchMode(true);
            }

            layout.addView(
                    webView,
                    new FrameLayout.LayoutParams(
                            LayoutParams.MATCH_PARENT,
                            LayoutParams.MATCH_PARENT,
                            Gravity.NO_GRAVITY)
            );

            mWebView = webView;
        }});
    }

    public void Destroy() {
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {public void run() {
            if (IsInitialized()){
                layout.removeView(mWebView);
                mWebView = null;
            }
        }});
    }

    public void LoadURL(final String url) {
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {public void run() {
            if (IsInitialized()){
                mWebView.loadUrl(url);
            }
        }});
    }

    public void SetMargins(int left, int top, int right, int bottom) {
        final FrameLayout.LayoutParams params  = new FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT,
                Gravity.NO_GRAVITY
        );
        params.setMargins(left, top, right, bottom);
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {public void run() {
            if (IsInitialized()){
                mWebView.setLayoutParams(params);
            }
        }});
    }

}
